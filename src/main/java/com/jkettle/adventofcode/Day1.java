package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/1
public class Day1 {

    private static final String INPUT_FILE_PATH = "day1.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day1.class);

    public static int calculateQ1Solution(List<Integer> depths) {

        return calculateDepthIncreases(depths);
    }

    public static int calculateQ2Solution(List<Integer> depths) {

        List<Integer> depthSums = new ArrayList<>();
        for (int i = 0; i < depths.size() - 2; i++) {
            int sum = depths.get(i) + depths.get(i + 1) + depths.get(i + 2);
            depthSums.add(sum);
        }
        LOGGER.info("Depth Sums List Size: {}", depthSums.size());
        return calculateDepthIncreases(depthSums);
    }

    public static int calculateDepthIncreases(List<Integer> depths) {
        int numberOfDepthIncreases = 0;
        Integer previousDepth = null;

        for (Integer currentDepth : depths) {

            if (previousDepth != null && currentDepth > previousDepth) {
                numberOfDepthIncreases++;
            }

            previousDepth = currentDepth;
        }

        return numberOfDepthIncreases;
    }

    public static void main(String[] args) throws IOException {

        //  List<Integer> list = Files.readAllLines(new File("day1/day1.txt").toPath(), Charset.defaultCharset())
        ClassLoader classLoader = Day1.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<Integer> list = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .map(Integer::valueOf)
                .collect(Collectors.toList());

        LOGGER.info("****** Day 1 ******");
        LOGGER.info("Input file lines: {}", list.size());
        LOGGER.info("Q1 Answer: {}", Day1.calculateQ1Solution(list));
        LOGGER.info("Q2 Answer: {}", Day1.calculateQ2Solution(list));
    }

}
