package com.jkettle.adventofcode;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.util.*;

// https://adventofcode.com/2021/day/10
public class Day10 {

    private static final String INPUT_FILE_PATH = "day10.txt";

    public static final int MAX = 9;

    private static final Logger LOGGER = LogManager.getLogger(Day10.class);

    private static final Map<Character, Character> matchingCharacters = Map.of('[', ']', '(', ')', '{', '}', '<', '>');
    private static final Map<Character, Integer> scoreMap = Map.of(')', 3, ']', 57, '}', 1197, '>', 25137);
    private static final Map<Character, Integer> autoCompleteScoreMap = Map.of(')', 1, ']', 2, '}', 3, '>', 4);

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day10.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .toList();

        LOGGER.info("****** Day 10 ******");
        LOGGER.info("Input file lines: {}", lines.size());

        int illegalScore = 0;
        List<Long> autoCompleteScore = new ArrayList<>();
        for (String line : lines) {

            List<Character> stack = new ArrayList<>();
            boolean invalidSequence = false;

            for (Character value : line.toCharArray()) {
                if (matchingCharacters.containsKey(value)) {
                    stack.add(value);
                } else if (matchingCharacters.containsValue(value)) {

                    char lastOpenCharacter = stack.get(stack.size() - 1);
                    char expectedCharacter = matchingCharacters.get(lastOpenCharacter);
                    if (expectedCharacter == value) {
                        stack.remove(stack.size() - 1);
                        continue;
                    }
                    invalidSequence = true;
                    illegalScore += scoreMap.get(value);
                    break;
                }
            }
            if (!invalidSequence && stack.size() > 0) {
                List<Character> complete = new ArrayList<>();
                for (int i = stack.size() - 1; i >= 0; i--)
                    complete.add(matchingCharacters.get(stack.get(i)));

                long score = 0;
                for (Character value : complete) {
                    score = (score * 5) + autoCompleteScoreMap.get(value);
                }
                autoCompleteScore.add(score);
            }
        }

        autoCompleteScore.sort(Long::compareTo);
        LOGGER.info("Q1 Answer: {}", illegalScore);
        LOGGER.info("Q2 Answer: {}", autoCompleteScore.get(autoCompleteScore.size() / 2));

    }
}
