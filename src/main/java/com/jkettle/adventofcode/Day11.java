package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;

// https://adventofcode.com/2021/day/11
public class Day11 {

    private static final String INPUT_FILE_PATH = "day11.txt";

    public static final int FLASH_IMMINENT = 9;

    public static int totalFlashes = 0;

    private static final Logger LOGGER = LogManager.getLogger(Day11.class);

    public static void calculateSolution(int[][] octopusMap) {

        int index = 1;
        while (true) {
            if (processStep(octopusMap)) {
                LOGGER.info("Q2 Answer: {}", index - 1);
                break;
            }

            if (index == 100) {
                LOGGER.info("Q1 Answer: {}", totalFlashes);
            }
            index++;
        }

    }

    private static boolean processStep(int[][] octopusMap) {

        boolean allZero = true;
        for (int i = 0; i < octopusMap.length; i++) {
            for (int j = 0; j < octopusMap[0].length; j++) {
                if (octopusMap[i][j] != 0) allZero = false;
                octopusMap[i][j]++;
            }
        }

        for (int i = 0; i < octopusMap.length; i++) {
            for (int j = 0; j < octopusMap[0].length; j++) {
                if (octopusMap[i][j] == 0) continue;
                if (octopusMap[i][j] > FLASH_IMMINENT) {
                    processFlash(octopusMap, i, j);
                }
            }
        }
        return allZero;

    }

    private static void processFlash(int[][] octopusMap, int i, int j) {

        totalFlashes++;
        int rowIndexEnd = Math.min(i + 2, octopusMap.length);
        int columnIndexEnd = Math.min(j + 2, octopusMap[0].length);

        octopusMap[i][j] = 0;
        for (int rowIndex = i == 0 ? 0 : i - 1; rowIndex < rowIndexEnd; rowIndex++) {
            for (int columnIndex = j == 0 ? 0 : j - 1; columnIndex < columnIndexEnd; columnIndex++) {

                if (rowIndex == i && columnIndex == j) continue;
                if (octopusMap[rowIndex][columnIndex] == 0) continue;

                if (++octopusMap[rowIndex][columnIndex] > FLASH_IMMINENT) {
                    processFlash(octopusMap, rowIndex, columnIndex);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day11.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .toList();

        LOGGER.info("****** Day 11 ******");
        LOGGER.info("Input file lines: {}", lines.size());

        int rowSize = lines.size();
        int columnSize = lines.get(0).length();

        int[][] octopusMap = new int[rowSize][columnSize];
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                octopusMap[i][j] = Integer.parseInt(String.valueOf(lines.get(i).charAt(j)));
            }
        }

        calculateSolution(octopusMap);
    }
}
