package com.jkettle.adventofcode;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

// https://adventofcode.com/2021/day/12
public class Day12 {

    private static final String INPUT_FILE_PATH = "day12.txt";

    private static final String START = "start";
    private static final String END = "end";

    private static Map<String, ArrayList<String>> caveMap;

    private static final Logger LOGGER = LogManager.getLogger(Day12.class);

    public static long calculateQ1Solution() {
        int sum = 0;
        return traverse(false, sum, new HashSet<>(), START);
    }

//    public static long calculateQ2Solution() {
//        int sum = 0;
//        return traverse(true, sum, new HashSet<>(), START);
//    }

    public static int traverse(boolean canVisitTwice, int sum, Set<String> visited, String currentCave) {

        if (currentCave.equals(END)) return 1;
        if (visited.contains(currentCave)) {
            if (currentCave.equals(START)) return 0;
            if (StringUtils.isAllLowerCase(currentCave)) {
                if (canVisitTwice) canVisitTwice = false;
                else return 0;
            }
        }

        visited.add(currentCave);
        for (String connection : caveMap.get(currentCave)) {
            sum += traverse(canVisitTwice, sum, visited, connection);
        }
        return sum;
    }

    private static Map<String, ArrayList<String>> initCaveData(List<String> lines) {

        Map<String, ArrayList<String>> caveMap = new HashMap<>();
        for (String line : lines) {
            String[] caves = line.split("-");
            String leftId = caves[0].trim();
            String rightId = caves[1].trim();
            caveMap.computeIfAbsent(leftId, k -> new ArrayList<>()).add(rightId);
            caveMap.computeIfAbsent(rightId, k -> new ArrayList<>()).add(leftId);
        }
        return caveMap;
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day12.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .toList();

        caveMap = initCaveData(lines);

        LOGGER.info("****** Day 12 ******");
        LOGGER.info("Input file lines: {}", lines.size());
        LOGGER.info("Q1 Answer: {}", Day12.calculateQ1Solution());
//        LOGGER.info("Q2 Answer: {}", Day12.calculateQ2Solution());
    }

}
