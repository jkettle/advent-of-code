package com.jkettle.adventofcode;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// https://adventofcode.com/2021/day/13
public class Day13 {

    private static final String FOLD_PREFIX = "fold along ";

    private static final String INPUT_FILE_PATH = "day13.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day13.class);

    public static long calculateQ1Solution(List<FoldAction> foldActions, String[][] plot) {

        for (FoldAction foldAction : foldActions) {

            if (foldAction.direction.equals("y")) {
                plot = foldUp(plot, foldAction.value);
            } else if (foldAction.direction.equals("x")) {
                plot = foldLeft(plot, foldAction.value);
            }

            LOGGER.debug("Post Fold {}", foldAction.direction);
            return Stream.of(plot)
                    .flatMap(Stream::of)
                    .filter(s -> s.equals("#"))
                    .count();
        }
        return 0;
    }

    public static long calculateQ2Solution(List<FoldAction> foldActions, String[][] plot) {
        for (FoldAction foldAction : foldActions) {
            if (foldAction.direction.equals("y")) {
                plot = foldUp(plot, foldAction.value);
            } else if (foldAction.direction.equals("x")) {
                plot = foldLeft(plot, foldAction.value);
            }
        }
        LOGGER.info("Q2 Answer: ");
        printPlot(plot);
        return 0;
    }


    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day13.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());

        List<FoldAction> foldActions = new ArrayList<>();
        List<Pair<Integer, Integer>> points = new ArrayList<>();

        int maxX = 0, maxY = 0;
        for (String line : lines) {
            if (!line.startsWith(FOLD_PREFIX)) {
                String[] snippets = line.split(",");
                int x = Integer.parseInt(snippets[0]);
                int y = Integer.parseInt(snippets[1]);
                if (x > maxX) maxX = x;
                if (y > maxY) maxY = y;
                points.add(Pair.of(x, y));

            } else {
                String[] foldActionSnippets = line.replaceAll(FOLD_PREFIX, "").trim().split("=");
                foldActions.add(new FoldAction(foldActionSnippets[0], Integer.parseInt(foldActionSnippets[1])));
            }
        }

        String[][] pointPlot = new String[maxY + 1][maxX + 1];
        Arrays.stream(pointPlot).forEach(a -> Arrays.fill(a, "."));
        points.forEach(point -> pointPlot[point.getRight()][point.getLeft()] = "#");

        LOGGER.info("****** Day 13 ******");
        LOGGER.info("Input file lines: {}", lines.size());
        LOGGER.info("Q1 Answer: {}", Day13.calculateQ1Solution(foldActions, pointPlot));
        Day13.calculateQ2Solution(foldActions, pointPlot);
    }

    public static void printPlot(String[][] pointPlot) {
        StringBuilder output = new StringBuilder();
        output.append(System.lineSeparator());
        for (String[] row : pointPlot) {
            for (String s : row) {
                output.append(s);
                output.append(" ");
            }
            output.append(System.lineSeparator());
        }
        LOGGER.info("{}", output.toString());
    }

    private static String[][] foldLeft(String[][] inputPlot, int foldIndex) {
        String[][] returnPlot = new String[inputPlot.length][foldIndex];
        for (int i = 0; i < inputPlot.length; i++) {
            returnPlot[i] = Arrays.copyOfRange(inputPlot[i], 0, foldIndex);
        }

        for (int i = foldIndex + 1; i < inputPlot[0].length; i++) {
            for (int j = 0; j < returnPlot.length; j++) {
                if (inputPlot[j][i].equals("#")) {
                    int newYValue = foldIndex - (i - foldIndex);
                    returnPlot[j][newYValue] = "#";
                }
            }
        }
        return returnPlot;
    }


    public static String[][] foldUp(String[][] inputPlot, int foldIndex) {
        String[][] returnPlot = new String[foldIndex][inputPlot[0].length];
        System.arraycopy(inputPlot, 0, returnPlot, 0, foldIndex);
        for (int i = 0; i < inputPlot[0].length; i++) {
            for (int j = foldIndex + 1; j < inputPlot.length; j++) {
                if (inputPlot[j][i].equals("#")) {
                    int newYValue = foldIndex - (j - foldIndex);
                    returnPlot[newYValue][i] = "#";
                }
            }
        }
        return returnPlot;
    }


    public static class FoldAction {

        String direction;
        int value;

        public FoldAction(String direction, int value) {
            this.direction = direction;
            this.value = value;
        }
    }
}
