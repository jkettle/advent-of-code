package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/2
public class Day2 {

    private static final String INPUT_FILE_PATH = "day2.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day2.class);


    public static int calculateQ1Solution(List<String> commands) {

        Location startLocation = new Location();
        for (String command : commands) {
            handleQ1Command(startLocation, command);
        }

        LOGGER.info("x: {}, y: {}", startLocation.getX(), startLocation.getY());
        return startLocation.getX() * startLocation.getY();
    }

    public static int calculateQ2Solution(List<String> commands) {
        Location startLocation = new Location();
        for (String command : commands) {
            handleQ2Command(startLocation, command);
        }

        LOGGER.info("x: {}, y: {}, aim: {}, depth {}", startLocation.getX(), startLocation.getY(), startLocation.getAim(), startLocation.getDepth());
        return startLocation.getX() * startLocation.getDepth();
    }

    public static void handleQ1Command(Location location, String command) {

        String[] commandSnippets = command.split(" ");
        Direction direction = Direction.valueOf(commandSnippets[0].toUpperCase(Locale.ROOT));
        int distance = Integer.parseInt(commandSnippets[1]);

        switch (direction) {
            case UP -> location.setY(location.getY() - distance);
            case DOWN -> location.setY(location.getY() + distance);
            case FORWARD -> location.setX(location.getX() + distance);
            default -> throw new IllegalArgumentException("Unrecognized direction");
        }
    }

    public static void handleQ2Command(Location location, String command) {

        String[] commandSnippets = command.split(" ");
        Direction direction = Direction.valueOf(commandSnippets[0].toUpperCase(Locale.ROOT));
        int distance = Integer.parseInt(commandSnippets[1]);

        switch (direction) {
            case UP -> location.setAim(location.getAim() - distance);
            case DOWN -> location.setAim(location.getAim() + distance);
            case FORWARD -> {
                location.setX(location.getX() + distance);
                location.setDepth(location.getDepth() + (distance * location.getAim()));
            }
            default -> throw new IllegalArgumentException("Unrecognized direction");
        }
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day2.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> list = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());

        LOGGER.info("****** Day 2 ******");
        LOGGER.info("Input file lines: {}", list.size());
        LOGGER.info("Q1 Answer: {}", Day2.calculateQ1Solution(list));
        LOGGER.info("Q2 Answer: {}", Day2.calculateQ2Solution(list));
    }

    enum Direction {
        UP, DOWN, FORWARD;
    }

    public static class Location {
        private int x;
        private int y;
        private int aim;
        private int depth;

        public Location() {
            this.x = 0;
            this.y = 0;
            this.aim = 0;
            this.depth = 0;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getAim() {
            return aim;
        }

        public void setAim(int aim) {
            this.aim = aim;
        }

        public int getDepth() {
            return depth;
        }

        public void setDepth(int depth) {
            this.depth = depth;
        }
    }

}
