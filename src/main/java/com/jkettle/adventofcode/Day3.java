package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/3
public class Day3 {

    private static final String INPUT_FILE_PATH = "day3.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day3.class);

    public static int calculateQ1Solution(List<String> reportSnippets) {

        Map<Integer, Integer> bitCounterMap = new HashMap<>();
        for (String reportSnippet : reportSnippets) {
            for (int i = 0; i < reportSnippet.length(); i++) {
                Integer bitValue = reportSnippet.charAt(i) == '1' ? 1 : 0;
                Integer previousValue = bitCounterMap.get(i) != null ? bitCounterMap.get(i) : 0;
                bitCounterMap.put(i, previousValue + bitValue);
            }
        }

        StringBuilder gammaValue = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry : bitCounterMap.entrySet()) {
            String prominentBitValue = entry.getValue() > (reportSnippets.size() / 2) ? "1" : "0";
            gammaValue.append(prominentBitValue);
        }

        int gammaIntValue = Integer.parseInt(gammaValue.toString(), 2);
        String epsilonValue = gammaValue.toString().replaceAll("0", "2")
                .replaceAll("1", "0")
                .replaceAll("2", "1");
        int epsilonIntValue = Integer.parseInt(epsilonValue, 2);
        return gammaIntValue * epsilonIntValue;
    }

    public static int calculateQ2Solution(List<String> reportSnippets) {

        Integer oxygenRating = null;
        Integer c02Rating = null;
        List<String> oxygenReportSnippets = new ArrayList<>(reportSnippets);
        List<String> c02ReportSnippets = new ArrayList<>(reportSnippets);
        int index = 0;
        while (oxygenRating == null || c02Rating == null) {

            if (oxygenReportSnippets.size() == 1) {
                oxygenRating = Integer.parseInt(oxygenReportSnippets.get(0), 2);
            } else {
                oxygenReportSnippets = getFilteredResults(oxygenReportSnippets, index, true);
            }

            if (c02ReportSnippets.size() == 1) {
                c02Rating = Integer.parseInt(c02ReportSnippets.get(0), 2);
            } else {
                c02ReportSnippets = getFilteredResults(c02ReportSnippets, index, false);
            }

            index++;
        }

        LOGGER.info("oxygenRating: {}, c02Rating: {}", Integer.toBinaryString(oxygenRating), Integer.toBinaryString(c02Rating));
        return oxygenRating * c02Rating;
    }

    public static List<String> getFilteredResults(List<String> reportSnippets, int index, boolean useCommon) {

        List<String> valueOneReportSnippets = new ArrayList<>();
        List<String> valueZeroReportSnippets = new ArrayList<>();

        double majority = ((double) reportSnippets.size()) / 2;
        for (String reportSnippet : reportSnippets) {
            if (reportSnippet.charAt(index) == '1') {
                valueOneReportSnippets.add(reportSnippet);
            } else {
                valueZeroReportSnippets.add(reportSnippet);
            }
        }

        if (valueOneReportSnippets.size() >= majority) {
            return useCommon ? valueOneReportSnippets : valueZeroReportSnippets;
        } else {
            return useCommon ? valueZeroReportSnippets : valueOneReportSnippets;
        }
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day3.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> list = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());

        LOGGER.info("****** Day 3 ******");
        LOGGER.info("Input file lines: {}", list.size());
        LOGGER.info("Q1 Answer: {}", Day3.calculateQ1Solution(list));
        LOGGER.info("Q2 Answer: {}", Day3.calculateQ2Solution(list));
    }

}
