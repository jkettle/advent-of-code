package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// https://adventofcode.com/2021/day/4
public class Day4 {

    private static final String INPUT_FILE_PATH = "day4.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day4.class);

    public static int calculateQ1Solution(BingoGame bingoGame) {

        for (int calledNumber : bingoGame.calledNumbers) {
            List<Board> boards = bingoGame.checkNumber(calledNumber);
            if (boards.size() > 0) {
                Board board = boards.get(0);
                int sum = board.getSumOfAllUnMarkedCells();
                LOGGER.info("Board Id {} is the winner! Called number: {}, unchecked sum: {}", board.getId(), calledNumber, sum);
                return calledNumber * sum;
            }
        }

        return 0;
    }

    public static int calculateQ2Solution(BingoGame bingoGame) {

        int index = 0;
        int returnValue = 0;
        for (int calledNumber : bingoGame.calledNumbers) {
            index++;
            List<Board> winningBoards = bingoGame.checkNumber(calledNumber);
            if (winningBoards.size() > 0) {

                for (Board board : winningBoards) {
                    int sum = board.getSumOfAllUnMarkedCells();
                    returnValue = calledNumber * sum;
                    LOGGER.info("Board Id {} is the winner! Called number: {}, unchecked sum: {}, returnValue: {}, index: {}", board.getId(), calledNumber, sum, returnValue, index);
                }
                bingoGame.getBoards().removeAll(winningBoards);
            }
        }
        return returnValue;
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day4.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> list = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());

        BingoGame bingoGame = new BingoGame();
        bingoGame.loadData(list);

        LOGGER.info("****** Day 4 ******");
        LOGGER.info("Input file lines: {}", list.size());
        LOGGER.info("Q1 Answer: {}", Day4.calculateQ1Solution(bingoGame));
        bingoGame.resetGame();
        LOGGER.info("Q2 Answer: {}", Day4.calculateQ2Solution(bingoGame));
    }

    private static class BingoGame {

        List<Integer> calledNumbers;
        List<Board> boards;

        public void resetGame() {
            boards.forEach(Board::markAllCellsAsUnchecked);
        }

        public void loadData(List<String> list) {

            List<String> calledNumbers = List.of(list.get(0).split(","));
            this.setCalledNumbers(calledNumbers
                    .stream()
                    .map(Integer::parseInt)
                    .collect(Collectors.toList())
            );

            list.remove(0);
            //list.remove(1);

            int boardId = 1;
            Board board = new Board(boardId);
            for (int i = 0; i <= list.size(); i++) {


                if (i == list.size()) continue;

                board.getCells().add(Stream.of(list.get(i).split(" "))
                        .filter(s -> !s.isBlank())
                        .map(s -> new Cell(Integer.parseInt(s)))
                        .collect(Collectors.toList()));

                if ((i + 1) % 5 == 0) {
                    this.getBoards().add(board);
                    board = new Board(++boardId);
                }
            }
        }

        public List<Board> checkNumber(int number) {
            List<Board> boards = new ArrayList<>();
            for (Board board : this.boards) {
                board.markCellsAsChecked(number);
                if (board.isWinner()) {
                    boards.add(board);
                }
            }
            return boards;
        }

        public BingoGame() {
            this.calledNumbers = new ArrayList<>();
            this.boards = new ArrayList<>();
        }

        public List<Integer> getCalledNumbers() {
            return calledNumbers;
        }

        public void setCalledNumbers(List<Integer> calledNumbers) {
            this.calledNumbers = calledNumbers;
        }

        public List<Board> getBoards() {
            return boards;
        }

        public void setBoards(List<Board> boards) {
            this.boards = boards;
        }
    }

    private static class Board {

        int id;
        List<List<Cell>> cells;

        public void markAllCellsAsUnchecked() {
            cells.stream()
                    .flatMap(List::stream)
                    .forEach(cell -> cell.setChecked(false));
        }

        public void markCellsAsChecked(int number) {
            cells.stream()
                    .flatMap(List::stream)
                    .filter(cell -> cell.getNumber() == number)
                    .forEach(cell -> cell.setChecked(true));
        }

        public int getSumOfAllUnMarkedCells() {
            return cells.stream()
                    .flatMap(List::stream)
                    .filter(cell -> !cell.isChecked())
                    .map(Cell::getNumber)
                    .mapToInt(Integer::intValue)
                    .sum();
        }

        public boolean isWinner() {

            Map<Integer, Boolean> columnWinnerMap = new HashMap<>(Map.of(
                    0, true,
                    1, true,
                    2, true,
                    3, true,
                    4, true
            ));
            for (List<Cell> row : cells) {
                boolean winningRow = true;
                for (int j = 0; j < row.size(); j++) {
                    Cell cell = row.get(j);
                    if (!cell.checked) {
                        winningRow = false;
                        columnWinnerMap.put(j, false);
                    }
                }
                if (winningRow) return true;
            }
            return columnWinnerMap.containsValue(true);
        }

        public Board(int id) {
            this.id = id;
            this.cells = new ArrayList<>();
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<List<Cell>> getCells() {
            return cells;
        }

        public void setCells(List<List<Cell>> cells) {
            this.cells = cells;
        }
    }

    private static class Cell {

        boolean checked;
        int number;

        public Cell(int number) {
            this.number = number;
            this.checked = false;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }
    }

}
