package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/5
public class Day5 {

    private static final String MAP_DELIMINATOR = "->";

    private static final String INPUT_FILE_PATH = "day5.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day5.class);

    public static int calculateQ1Solution(VentRadarMap ventRadarMap) {
        ventRadarMap.calculateStraightRadarMap();
        return ventRadarMap.dangerousPointNumber;
    }

    public static int calculateQ2Solution(VentRadarMap ventRadarMap) {
        ventRadarMap.calculateDiagonalRadarMap();
        return ventRadarMap.dangerousPointNumber;
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day5.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());

        LOGGER.info("****** Day 5 ******");
        LOGGER.info("Input file lines: {}", lines.size());
        VentRadarMap radarMap = new VentRadarMap();
        radarMap.loadData(lines);
        LOGGER.info("Q1 Answer: {}", Day5.calculateQ1Solution(radarMap));
        LOGGER.info("Q2 Answer: {}", Day5.calculateQ2Solution(radarMap));

        StringBuilder output = new StringBuilder();
        output.append(System.lineSeparator());
        for (List<Integer> row : radarMap.radarMap) {
            output.append(row.stream().map(String::valueOf).collect(Collectors.joining(",")));
            output.append(System.lineSeparator());
        }
        if (lines.size() < 20) {
            LOGGER.info("{}", output);
        }
    }

    private static class VentRadarMap {

        List<VentLine> ventLines = new ArrayList<>();
        List<List<Integer>> radarMap = new ArrayList<>();

        int maxX, maxY;
        int dangerousPointNumber = 0;

        public void resetMap() {
            dangerousPointNumber = 0;
            radarMap.stream()
                    .flatMap(Collection::stream)
                    .forEach(o -> o = 0);
        }

        public void loadData(List<String> lines) {

            for (String line : lines) {
                String[] values = line.split(MAP_DELIMINATOR);
                if (values.length != 2) {
                    throw new IllegalArgumentException("Malformed input data");
                }

                String[] firstValues = values[0].split(",");
                int x1 = Integer.parseInt(firstValues[0].trim());
                int y1 = Integer.parseInt(firstValues[1].trim());
                String[] secondValues = values[1].split(",");
                int x2 = Integer.parseInt(secondValues[0].trim());
                int y2 = Integer.parseInt(secondValues[1].trim());

                ventLines.add(new VentLine(x1, y1, x2, y2));
                if (x1 > maxX) maxX = x1;
                if (x2 > maxX) maxX = x2;
                if (y1 > maxX) maxY = y1;
                if (y2 > maxX) maxY = y2;
            }

            LOGGER.info("Data Loaded. Max X Value: {}, Max Y Value: {}", maxX, maxY);
            List<Integer> row = new ArrayList<>(Collections.nCopies(maxX + 1, 0));
            for (int i = 0; i <= maxY; i++) {
                radarMap.add(new ArrayList<>(row));
            }
        }

        public void calculateDiagonalRadarMap() {
            for (VentLine ventLine : ventLines) {
                if (ventLine.x1 != ventLine.x2 && ventLine.y1 != ventLine.y2) {
                    int xStep = ventLine.x1 > ventLine.x2 ? -1 : 1;
                    int yStep = ventLine.y1 > ventLine.y2 ? -1 : 1;
                    int xIndex = ventLine.x1;
                    int yIndex = ventLine.y1;
                    int totalSteps = Math.abs(ventLine.x1 - ventLine.x2);
                    for (int i = 0; i <= totalSteps; i++) {
                        int value = radarMap.get(yIndex).get(xIndex) + 1;
                        radarMap.get(yIndex).set(xIndex, value);
                        if (value == 2) {
                            dangerousPointNumber++;
                        }
                        xIndex = xIndex + xStep;
                        yIndex = yIndex + yStep;
                    }
                }
            }
        }

        public void calculateStraightRadarMap() {
            for (VentLine ventLine : ventLines) {
                if (ventLine.x1 == ventLine.x2) {

                    int start = Math.min(ventLine.y1, ventLine.y2);
                    int end = Math.max(ventLine.y1, ventLine.y2);
                    for (int i = start; i <= end; i++) {
                        int value = radarMap.get(i).get(ventLine.x1) + 1;
                        radarMap.get(i).set(ventLine.x1, value);
                        if (value == 2) {
                            dangerousPointNumber++;
                        }
                    }

                } else if (ventLine.y1 == ventLine.y2) {

                    int start = Math.min(ventLine.x1, ventLine.x2);
                    int end = Math.max(ventLine.x1, ventLine.x2);
                    for (int i = start; i <= end; i++) {
                        int value = radarMap.get(ventLine.y1).get(i) + 1;
                        radarMap.get(ventLine.y1).set(i, value);
                        if (value == 2) {
                            dangerousPointNumber++;
                        }
                    }
                }

            }
        }

    }

    private static class VentLine {

        int x1, x2, y1, y2;

        public VentLine(int x1, int y1, int x2, int y2) {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
        }
    }

}
