package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/6
public class Day6 {

    private static final String INPUT_FILE_PATH = "day6.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day6.class);

    public static long calculateQ1Solution(long[] generations) {
        return fishAfterDay(80, generations);
    }

    public static long calculateQ2Solution(long[] generations) {
        return fishAfterDay(256, generations);
    }

    private static long fishAfterDay(int totalDays, long[] inputDaysLeft) {
        long[] daysLeft = Arrays.copyOf(inputDaysLeft, inputDaysLeft.length);
        for (int i = 0; i < totalDays; i++) {
            long parents = daysLeft[0];
            for (int n = 1; n < daysLeft.length; n++) {
                daysLeft[n - 1] = daysLeft[n];
            }
            daysLeft[6] += parents;
            daysLeft[8] = parents;
        }
        return Arrays.stream(daysLeft).sum();
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day6.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());


        long[] daysLeft = new long[9];
        for (String timerValue : lines.get(0).split(",")) {
            daysLeft[Integer.parseInt(timerValue)]++;
        }

        LOGGER.info("****** Day 6 ******");
        LOGGER.info("Input file lines: {}", lines.size());
        LOGGER.info("Q1 Answer: {}", Day6.calculateQ1Solution(daysLeft));
        LOGGER.info("Q2 Answer: {}", Day6.calculateQ2Solution(daysLeft));
    }

}
