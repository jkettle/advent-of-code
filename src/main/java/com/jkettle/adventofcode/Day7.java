package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/7
public class Day7 {

    private static final String INPUT_FILE_PATH = "day7.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day7.class);

    public static int calculateQ1Solution(List<Integer> list) {

        int size = list.size();
        int median = (int) list.stream()
                .mapToDouble(value -> (double) value)
                .skip((size - 1) / 2)
                .limit(2 - size % 2)
                .average()
                .orElse(Double.NaN);

        int fuelCost = 0;
        for (int individualPosition : list) {
            fuelCost += Math.abs(individualPosition - median);
        }
        return fuelCost;
    }

    public static long calculateQ2Solution(List<Integer> list) {

        int fuelCost = 0;
        Integer mean = (list.stream().reduce(0, Integer::sum)) / list.size();
        for (Integer position : list) {
            // https://www.mathsisfun.com/algebra/triangular-numbers.html
            int n = Math.abs(position - mean);
            fuelCost += (((n * n) + n) / 2);
        }
        return fuelCost;
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day7.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());


        List<Integer> horizontalPositions = new ArrayList<>();
        for (String value : lines.get(0).split(",")) {
            horizontalPositions.add(Integer.parseInt(value));
        }
        horizontalPositions = horizontalPositions.stream().sorted().collect(Collectors.toList());

        LOGGER.info("****** Day 7 ******");
        LOGGER.info("Input file lines: {}", lines.size());
        LOGGER.info("Q1 Answer: {}", Day7.calculateQ1Solution(horizontalPositions));
        LOGGER.info("Q2 Answer: {}", Day7.calculateQ2Solution(horizontalPositions));
    }

}
