package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/8
public class Day8 {

    private static final String INPUT_FILE_PATH = "day8.txt";

    private static final Logger LOGGER = LogManager.getLogger(Day8.class);

    private static final List<Integer> UNIQUE_NUMBER_SIZES = List.of(2, 4, 3, 7); // 1, 4, 7, 8

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day8.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .collect(Collectors.toList());

        LOGGER.info("****** Day 8 ******");
        LOGGER.info("Input file lines: {}", lines.size());

        AtomicInteger total = new AtomicInteger();
        AtomicInteger uniqueNumberCount = new AtomicInteger();
        lines.forEach(s -> {
            String[] inputs = s.split("\\|");
            List<String> inputValues = List.of(inputs[0].trim().split(" "));
            List<String> outputValues = List.of(inputs[1].trim().split(" "));
            outputValues.forEach(s1 -> {
                if (UNIQUE_NUMBER_SIZES.contains(s1.length())) {
                    uniqueNumberCount.getAndIncrement();
                }
            });

            Set<Character> oneCharacters = null;
            Set<Character> fourCharacters = null;
            for (String input : inputValues) {
                if (input.length() == 2) {
                    oneCharacters = input.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
                }
                if (input.length() == 4) {
                    fourCharacters = input.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
                }
            }

            total.addAndGet(convertOutputToInt(outputValues, oneCharacters, fourCharacters));
        });

        LOGGER.info("Q1 Answer: {}", uniqueNumberCount);
        LOGGER.info("Q2 Answer: {}", total);
    }

    private static int convertOutputToInt(List<String> outputValues, Set<Character> oneCharacters, Set<Character> fourCharacters) {

        StringBuilder output = new StringBuilder();
        for (String outputValue : outputValues) {

            Set<Character> outputChars = outputValue.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());

            Set<Character> onesCommon = new HashSet<>(oneCharacters);
            Set<Character> foursCommon = new HashSet<>(fourCharacters);

            onesCommon.retainAll(outputChars);
            foursCommon.retainAll(outputChars);

            switch (outputValue.length()) {
                case 2 -> output.append("1");
                case 3 -> output.append("7");
                case 4 -> output.append("4");
                case 7 -> output.append("8");
                case 5 -> {
                    if (foursCommon.size() == 2) output.append("2");
                    else if (foursCommon.size() == 3) {
                        if (onesCommon.size() == 1) output.append("5");
                        else if (onesCommon.size() == 2) output.append("3");
                    }
                }
                case 6 -> {
                    if (foursCommon.size() == 4) output.append("9");
                    else if (foursCommon.size() == 3) {
                        if (onesCommon.size() == 1) output.append("6");
                        else if (onesCommon.size() == 2) output.append("0");

                    }
                }
                default -> throw new IllegalStateException("Unexpected value: " + outputValue.length());
            }
        }

        return Integer.parseInt(output.toString());
    }

}
