package com.jkettle.adventofcode;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

// https://adventofcode.com/2021/day/9
public class Day9 {

    private static final String INPUT_FILE_PATH = "day9.txt";

    public static final int MAX = 9;

    private static final Logger LOGGER = LogManager.getLogger(Day9.class);

    public static Set<Pair<Integer, Integer>> calculateQ1Solution(int[][] heightMap) {

        Set<Pair<Integer, Integer>> lowestPoints = new HashSet<>();
        Set<Pair<Integer, Integer>> alreadySearchedPoints = new HashSet<>();

        int sum = 0;
        int rowSize = heightMap.length;
        int columnSize = heightMap[0].length;
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < columnSize; j++) {

                int lowest = heightMap[i][j];
                Pair<Integer, Integer> currentPoint = Pair.of(j, i);
                if (alreadySearchedPoints.contains(currentPoint)) continue;
                alreadySearchedPoints.add(Pair.of(j, i));

                Pair<Integer, Integer> previousLowestPoint = currentPoint;
                while (true) {
                    Optional<Pair<Integer, Integer>> nextLowestPointWrapper = getNextLowestAdjacentPoint(previousLowestPoint, heightMap);
                    if (nextLowestPointWrapper.isEmpty()) {
                        lowestPoints.add(previousLowestPoint);
                        int lowPointValue = heightMap[previousLowestPoint.getRight()][previousLowestPoint.getLeft()];
                        sum += (1 + lowPointValue);
                        break;
                    }
                    Pair<Integer, Integer> nextLowestPoint = nextLowestPointWrapper.get();
                    if (alreadySearchedPoints.contains(nextLowestPoint)) {
                        break;
                    }
                    alreadySearchedPoints.add(nextLowestPoint);
                    previousLowestPoint = nextLowestPoint;
                }
            }
        }
        LOGGER.info("Q1 Answer: {}", sum);
        return lowestPoints;
    }

    private static int calculateQ2Solution(int[][] heightMap, Set<Pair<Integer, Integer>> lowPoints) {

        Set<Pair<Integer, Integer>> claimedPoints = new HashSet<>();
        List<Integer> basinSizes = new ArrayList<>();

        for (Pair<Integer, Integer> lowPoint : lowPoints) {
            Set<Pair<Integer, Integer>> pointsInBasin = getBasinPoints(lowPoint, heightMap, claimedPoints);
            claimedPoints.addAll(pointsInBasin);
            basinSizes.add(pointsInBasin.size());
        }

        return basinSizes.stream()
                .sorted(Collections.reverseOrder())
                .mapToInt(Integer::intValue)
                .limit(3)
                .reduce(1, (a, b) -> a * b);
    }

    private static Set<Pair<Integer, Integer>> getBasinPoints(Pair<Integer, Integer> lowPoint, int[][] heightMap, Set<Pair<Integer, Integer>> claimedPoints) {

        LinkedList<Pair<Integer, Integer>> basinPoints = new LinkedList<>(Set.of(lowPoint));
        Set<Pair<Integer, Integer>> collectedPoints = new HashSet<>();

        while (!basinPoints.isEmpty()) {
            Pair<Integer, Integer> nextPoint = basinPoints.remove();
            collectedPoints.add(nextPoint);
            Set<Pair<Integer, Integer>> foundPoints = getAllHigherAdjacentPoint(collectedPoints, nextPoint, heightMap);
            basinPoints.addAll(foundPoints);
        }
        return collectedPoints;
    }


    private static Set<Pair<Integer, Integer>> getAllHigherAdjacentPoint(Set<Pair<Integer, Integer>> collectedPoints, Pair<Integer, Integer> point, int[][] heightMap) {

        int leftIndex = point.getLeft() - 1;
        int rightIndex = point.getLeft() + 1;
        int upIndex = point.getRight() - 1;
        int downIndex = point.getRight() + 1;

        Set<Pair<Integer, Integer>> adjacentPoints = new HashSet<>();
        int lowest = heightMap[point.getRight()][point.getLeft()];
        if (leftIndex >= 0) {
            Pair<Integer, Integer> newPair = Pair.of(leftIndex, point.getRight());
            int value = heightMap[point.getRight()][leftIndex];
            if (value != MAX && value > lowest) {
                adjacentPoints.add(newPair);
            }
        }
        if (rightIndex < heightMap[0].length) {
            Pair<Integer, Integer> newPair = Pair.of(rightIndex, point.getRight());
            int value = heightMap[point.getRight()][rightIndex];
            if (value != MAX && value > lowest) {
                adjacentPoints.add(newPair);
            }
        }
        if (upIndex >= 0) {
            Pair<Integer, Integer> newPair = Pair.of(point.getLeft(), upIndex);
            int value = heightMap[upIndex][point.getLeft()];
            if (value != MAX && value > lowest) {
                adjacentPoints.add(newPair);
            }
        }
        if (downIndex < heightMap.length) {
            Pair<Integer, Integer> newPair = Pair.of(point.getLeft(), downIndex);
            int value = heightMap[downIndex][point.getLeft()];
            if (value != MAX && value > lowest) {
                adjacentPoints.add(newPair);
            }
        }
        adjacentPoints.removeAll(collectedPoints);
        return adjacentPoints;
    }

    private static Optional<Pair<Integer, Integer>> getNextLowestAdjacentPoint(Pair<Integer, Integer> point, int[][] heightMap) {

        int leftIndex = point.getLeft() - 1;
        int rightIndex = point.getLeft() + 1;
        int upIndex = point.getRight() - 1;
        int downIndex = point.getRight() + 1;

        int lowest = heightMap[point.getRight()][point.getLeft()];
        Optional<Pair<Integer, Integer>> returnPointWrapper = Optional.empty();
        if (leftIndex >= 0) {
            int value = heightMap[point.getRight()][leftIndex];
            if (value <= lowest) {
                lowest = value;
                returnPointWrapper = Optional.of(Pair.of(leftIndex, point.getRight()));
            }
        }
        if (rightIndex < heightMap[0].length) {
            int value = heightMap[point.getRight()][rightIndex];
            if (value <= lowest) {
                lowest = value;
                returnPointWrapper = Optional.of(Pair.of(rightIndex, point.getRight()));
            }
        }
        if (upIndex >= 0) {
            int value = heightMap[upIndex][point.getLeft()];
            if (value <= lowest) {
                lowest = value;
                returnPointWrapper = Optional.of(Pair.of(point.getLeft(), upIndex));
            }
        }
        if (downIndex < heightMap.length) {
            int value = heightMap[downIndex][point.getLeft()];
            if (value <= lowest) {
                lowest = value;
                returnPointWrapper = Optional.of(Pair.of(point.getLeft(), downIndex));
            }
        }
        return returnPointWrapper;
    }

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Day9.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(INPUT_FILE_PATH)).getFile());
        List<String> lines = Files.readAllLines(file.toPath())
                .stream()
                .filter(s -> !s.isBlank())
                .toList();

        int rowSize = lines.size();
        int columnSize = lines.get(0).length();

        int[][] heightMap = new int[rowSize][columnSize];
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                heightMap[i][j] = Integer.parseInt(String.valueOf(lines.get(i).charAt(j)));
            }
        }

        LOGGER.info("****** Day 9 ******");
        LOGGER.info("Input file lines: {}", lines.size());
        Set<Pair<Integer, Integer>> lowPoints = Day9.calculateQ1Solution(heightMap);
        LOGGER.info("Q2 Answer: {}", Day9.calculateQ2Solution(heightMap, lowPoints));


        StringBuilder output = new StringBuilder();
        output.append(System.lineSeparator());
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                output.append(heightMap[i][j]);
            }
            output.append(System.lineSeparator());
        }
        if (lines.size() < 10) {
            LOGGER.info("{}", output);
        }
    }
}
