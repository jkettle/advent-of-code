package com.jkettle.adventofcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Helper {

    private static final Logger LOGGER = LogManager.getLogger(Helper.class);

    public static void print2dArray(int[][] array) {

        if (array.length == 0) return;

        StringBuilder output = new StringBuilder();
        output.append(System.lineSeparator());
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                output.append(array[i][j]);
            }
            output.append(System.lineSeparator());
        }
        LOGGER.info("{}", output.toString());
    }

}
